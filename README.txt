CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 

INTRODUCTION
---------------------

This module provides the functionality of stoping the execution of the script
in twig file. Like we do in PHP through die function.

Just use {{ die_in_twig() }} in any '.html.twig' template and it will end the
script's execution with a message 'End of script'.

REQUIREMENTS
---------------------

No special requirements.


INSTALLATION
---------------------

Install as you would normally install a Drupal contributed module.


CONFIGURATION
---------------------

No configuration required.


MAINTAINERS
---------------------

Current maintainer:
  * Akansha Saxena : (https://www.drupal.org/u/saxenaakansha30)
